import React from 'react';
import { withGoogleMap, GoogleMap, DirectionsRenderer } from 'react-google-maps';

class DrivingRoute extends React.Component {
  constructor(props) {
    super(props);

    const mapRoute = props.state.routes.find((r) => r.id === props.id);
    const start = mapRoute.start.split(',');
    const end = mapRoute.end.split(',');

    this.state = {
      origin: new google.maps.LatLng(start[0], start[1]),
      destination: new google.maps.LatLng(end[0], end[1]),
      directions: null
    };
  };

  componentDidMount() {
    const DirectionsService = new google.maps.DirectionsService();

    DirectionsService.route({
      origin: this.state.origin,
      destination: this.state.destination,
      travelMode: google.maps.TravelMode.DRIVING
    }, (result, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        this.setState({
          directions: result
        });
      } else {
        console.error(`error fetching directions ${result}`);
      }
    });
  };

  render() {
    const Map = withGoogleMap(props => (
      <GoogleMap
        defaultZoom={7}
        defaultCenter={this.state.origin}
      >
        {this.state.directions && <DirectionsRenderer directions={this.state.directions} />}
      </GoogleMap>
    ));

    return (
      <div className="driving-route">
        <Map
          containerElement={
            <div className="route-map" />
          }
          mapElement={
            <div style={{ height: '100%' }} />
          }
        />
      </div>
    );
  };
};

export default DrivingRoute;
