import React from 'react';

const Error404 = () => (
  <div className="error_404">
    <div>
      <h1>404</h1>
      <h2>Page not found.</h2>
    </div>
  </div>
);

export default Error404;
