import React from 'react';

const Home = ({ routes, start, end, formSubmit, inputChange, removeHandler }) => (
  <div className="home">
    <form onSubmit={formSubmit}>
      <div className="wrapper">

        <div className="field">
          <input
            type="text"
            name="start"
            value={start}
            onChange={inputChange}
            placeholder="44.7866, 20.4489"
            spellCheck="false"
          />
          <span />
        </div>

        <div className="field">
          <input
            type="text"
            name="end"
            value={end}
            onChange={inputChange}
            placeholder="45.2671, 19.8335"
            spellCheck="false"
          />
          <span />
        </div>

        <button type="submit">Find route</button>
      </div>
    </form>

    <section>
      <div className="wrapper">
        {routes.length
          ? <ol>
              {
                routes.map((route) => (
                  <li key={route.id}>
                    <a className="route" href={`/route/${route.id}`}>
                      {route.start} - {route.end}
                    </a>
                    <a className="remove" data-id={route.id} onClick={removeHandler}>Remove</a>
                  </li>
                ))
              }
            </ol>
          : <p className="empty">No previously entered routes.</p>
        }
      </div>
    </section>
  </div>
);

export default Home;
