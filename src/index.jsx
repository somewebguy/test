import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { matchPath } from 'react-router';
import Home from './components/home.jsx';
import Error404 from './components/error.jsx';
import DrivingRoute from './components/drivingRoute.jsx';

import './style.scss';

const routes = [
  '/',
  '/route/:id'
];

const routeMatch = routes.reduce((acc, route) => matchPath(window.location.pathname, route, { exact: true }) || acc, null);

const guid = () => {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
};

const App = React.createClass({
  getInitialState: () => {
    const storedRoutes = JSON.parse(sessionStorage.getItem('routes'));

    return {
      routes: storedRoutes || [],
      start: '',
      end: ''
    };
  },

  handleInput: function(event){
    const data = {};

    data[event.target.name] = event.target.value;
    this.setState(data);
  },

  handleSubmit: function(event){
    event.preventDefault();

    const routes = this.state.routes;
    const start = this.state.start;
    const end = this.state.end;
    const id = guid();

    if (start && end) {
      routes.push({
        start: this.state.start,
        end: this.state.end,
        id: id
      });

      this.setState({
        routes: routes,
        start: '',
        end: ''
      });

      sessionStorage.setItem('routes', JSON.stringify(routes));

      window.location = `${window.location.origin}/route/${id}`;
    }
  },

  handleRemove: function(event){
    if (confirm('Are you sure you want to remove this route?')) {
      const id = event.target.dataset.id;

      const routes = this.state.routes.filter((r) => {
        return r.id !== id;
      });

      this.setState({ routes: routes });
      sessionStorage.setItem('routes', JSON.stringify(routes));
    }
  },

  render: function(){
    return (
      <div>
        {routeMatch.isExact
          ? routeMatch.path === '/'
            ? <Home
                routes={this.state.routes}
                start={this.state.start}
                end={this.state.end}
                formSubmit={this.handleSubmit}
                inputChange={this.handleInput}
                removeHandler={this.handleRemove}
              />
            : <DrivingRoute
                state={this.state}
                id={routeMatch.params.id}
              />
          : <Error404 />
        }
      </div>
    );
  }
});

ReactDOM.render(<App />, document.getElementById('root'));
